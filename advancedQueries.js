// [ SECTION] - comparison query operators

// $gt/$gte operator
/*
	- Allows us to find documents that have field number valuies greater than or euqal to a specific value.

	- Sntax:
		db.collectionName.find({field : {$gt : value}});
		db.collectionName.find({field : {$gte : value}});
*/

db.users.find({age : {$gt : 50}}); // greater than   age 50
db.users.find({age : {$gte : 50}}); // greater than or equal to age 50

// $lt/$lte operator
/*
	- Allows us to find documents that have field value less tha nor equal to a specified value

	- Syntax:
		db.users.find({age : {$lt : 50}});
		db.users.find({age : {$lte : 50}});
*/

// $ne operator
/*
	- Allows us to find documents that have values not equal to a specified value.

	- Syntax:
		db.collectionName.find({age : {$ne : value}});
*/
db.users.find({age : {$ne : 82}});

// $in operator
/*
	- Allows us to find documents with specific match criteria on one field using different values

	- Syntax:
		db.collectioNName.find({field : {$in : value}});
*/
db.users.find({lastName : {$in : ["Hawking" , "Doe"]}});
db.users.find({courses : {$in : ["HTML" , "React"]}});

// [SECTION] - Logical Query Operators

// $or operator
/*


	- Styntax: db.collectionNmae.find({$or: [{fieldA: valueA}, {fieldB: valueB}]});
*/

// multiple filed value pairs
db.users.find({$or : [{firstName: "Niel"}, {age: "25"}]});
db.users.find({$or : [{firstName: "Niel"}, {age: {$gt: 30}}]});

// $and Operator
/*
	- Allows us to find documents matching multiple criteria in a single field.

	- Syntax: db.collectionNmae.find({$in: [{fieldA: valueA}, {fieldB: valueB}]});
*/
db.users.find({$and : [{age: {$ne: 82}}, {age: {$ne: 76}}]});

// [SECTION] - Field projection

// Inclusion
/*
	- Allows us to include/add specific field only when retrieving documents
	- The value provided is 1. To denote that the field is being included.
	- Syntax:
		db.collectionName.find({criteria}, {field:1});
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

// Exclusion
/*
	- Allows us to exclude/remove specific fields only when retrieving documents
	- The value provided is 0. To denote that the field is being excluded.
	-Syntax:
		db.collectionName.find({criteria}, {field:0});
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		contact: 0,
		departments: 0
	}
);

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
        _id: 0
	}
);

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1,
        _id: 0
	}
);

db.users.find(
	{
		firstName: "Jane"
	},
	{
		"contact.phone": 0
	}
);

// $slice operator - allows us to rertrieve only 1 element that matched the search criteria

db.users.find(
	{
		"namearr":
			{
				namea: "Juan"
			}
	},
	{
		namearr: {$slice: 1}
	}
);

// [SECTION] - Evaluation Query Operators

// $regex operator
/*
	- Allows us to find documents that match a specific string pattern using a regular expression
	- Syntax:
		db.users.find({field: $regex 'pattern', $options: '$optionValue'});
*/
db.users.find({firstName: {$regex: "N"}});

// Case insensitive query
db.users.find({firstName: {$regex: "j", $options: "$i"}});
